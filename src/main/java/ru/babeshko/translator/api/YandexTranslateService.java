package ru.babeshko.translator.api;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import ru.babeshko.translator.model.yandex.YandexTranslateRequest;
import ru.babeshko.translator.model.yandex.YandexTranslateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@ApplicationScoped
@Path("/translate")
@RegisterRestClient
public interface YandexTranslateService {

    @POST
    YandexTranslateResponse getTranslate(
            @HeaderParam("Authorization") String authHeader,
            YandexTranslateRequest request);

}

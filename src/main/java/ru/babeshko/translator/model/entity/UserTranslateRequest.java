package ru.babeshko.translator.model.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class UserTranslateRequest extends PanacheEntity {

    @Column
    public Date requestDate;

    @Column
    public String stringIn;

    @Column
    public String stringOut;

    @Column
    public String langParam;

    @Column
    public String ip;

}
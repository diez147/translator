package ru.babeshko.translator.model.entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Vocabulary extends PanacheEntity {

    @Column
    public String rootWord;

    @Column
    public String translatedWord;

}

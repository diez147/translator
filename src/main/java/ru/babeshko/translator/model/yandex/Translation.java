package ru.babeshko.translator.model.yandex;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Translation {

    @JsonProperty("text")
    private String text;

    @JsonProperty("detectedLanguageCode")
    private String detectedLanguageCode;

}

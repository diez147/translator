package ru.babeshko.translator.model.yandex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class YandexTranslateRequest {

    private String targetLanguageCode;

    private String texts;

    private String folderId;

}

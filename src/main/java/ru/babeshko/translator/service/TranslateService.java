package ru.babeshko.translator.service;

import io.quarkus.hibernate.reactive.panache.common.runtime.ReactiveTransactional;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.hibernate.reactive.mutiny.Mutiny;
import ru.babeshko.translator.api.YandexTranslateService;
import ru.babeshko.translator.model.entity.UserTranslateRequest;
import ru.babeshko.translator.model.entity.Vocabulary;
import ru.babeshko.translator.model.yandex.Translation;
import ru.babeshko.translator.model.yandex.YandexTranslateRequest;
import ru.babeshko.translator.model.yandex.YandexTranslateResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;

@ApplicationScoped
public class TranslateService {

    private final static String YANDEX_KEY = ConfigProvider.getConfig().getValue("yandex.key", String.class);

    private final static String IAM_TOKEN = "Bearer " + YANDEX_KEY;

    private String result = "";

    @ConfigProperty(name = "yandex.folder")
    String FOLDER_ID;

    @Inject
    @RestClient
    YandexTranslateService yandexTranslateService;

    @Inject
    Mutiny.SessionFactory sf;

    private Date getRequestDate() {
        return Calendar.getInstance().getTime();
    }

    public Uni<Vocabulary> saveVocab(String root, String translated) {
        Vocabulary vocabulary = new Vocabulary(root, translated);
        return sf.withTransaction(vocab -> vocab.persist(vocabulary)).replaceWith(vocabulary);
    }

    public Uni<UserTranslateRequest> saveUserRequest(
            Date date,
            String in,
            String out,
            String langParam,
            String ip) {
        UserTranslateRequest userTranslateRequest = new UserTranslateRequest(date, in, out, langParam, ip);
        return sf.withTransaction(userRequest -> userRequest
                .persist(userTranslateRequest))
                .replaceWith(userTranslateRequest);
    }

    @ReactiveTransactional
    public Uni<String> doTranslate(String text, String lang, String requestIp) {
        result = "";
        String[] words = text.split("\\s");
        for (String word : words) {
            YandexTranslateRequest request = new YandexTranslateRequest(lang, word, FOLDER_ID);
            Uni<YandexTranslateResponse> response = Uni.createFrom()
                    .item(yandexTranslateService.getTranslate(IAM_TOKEN, request));
            response.onItem()
                    .transform(translation -> translation.getTranslations()
                            .stream()
                            .map(Translation::getText)
                            .collect(Collectors.toList()).toString())
                    .subscribe()
                    .with(s -> result += s.replaceAll("[\\[\\]]", "") + " ");
            response.onItem()
                    .transform(translation -> translation.getTranslations()
                            .stream()
                            .map(Translation::getText)
                            .collect(Collectors.toList()).toString())
                    .call(s -> saveVocab(word.replaceAll("[()\\[\\]]", "")
                                    .replaceAll("[-+.^:,!?]", "")
                                    .toLowerCase(Locale.ROOT),
                            s.replaceAll("[-+.^:,!?]", "")
                                    .replaceAll("[()\\[\\]]", "")
                                    .toLowerCase(Locale.ROOT)))
                    .subscribe()
                    .with(s -> System.out.println(s + " word was added in vocab"));
        }
        Uni.createFrom().nullItem()
                .onItem()
                .call(userTranslateRequest -> saveUserRequest(getRequestDate(), text, result, lang, requestIp))
                .subscribe()
                .with(o -> System.out.println("Request with text: " + "<" + text + ">" +" was added in database"));
        return Uni.createFrom().item(result);
    }

}
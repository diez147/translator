package ru.babeshko.translator.resource;

import io.smallrye.mutiny.Uni;
import ru.babeshko.translator.service.TranslateService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@ApplicationScoped
@Path("/translate")
public class TranslateResource {

    @Inject
    TranslateService translateService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> translate(
            @QueryParam("text") String text,
            @QueryParam("langTo") String lang,
            @Context io.vertx.core.http.HttpServerRequest httpRequest) {
        return translateService.doTranslate(text, lang, httpRequest.remoteAddress().toString());
    }

}